import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../Modelo/Persona';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });

  constructor(private http:HttpClient) { }

  Url='https://factura.omegas-apps.com:3005/administracion/';

  getPersonas(){
    return this.http.get<any>(this.Url + 'obtener_personas');
  }

  createPersona(persona:any): Observable<any>  {
     let fecha: any = persona.fecha_nacimiento;
     let {year, month, day} = fecha;
     persona.fecha_nacimiento = `${year}-${month}-${day}`;

    return this.http.post<any>(this.Url + 'guardar_persona',persona,{ headers: this.headers });
  }

  getPersonaId(cedula:any){
    return this.http.post<any>(this.Url+"obtener_persona", cedula, { headers: this.headers });
  }

  updatePersona(persona:any){
    let fecha: any = persona.fecha_nacimiento;
    let {year, month, day} = fecha;
    persona.fecha_nacimiento = `${year}-${month}-${day}`;

    return this.http.post<Persona>(this.Url+"actualizar_persona", persona,{ headers: this.headers });
  }

  deletePersona(persona:any): Observable<any> {
    return this.http.post<any>(this.Url+"eliminar_persona",persona,{ headers: this.headers });
  }
}
